$('document').ready(function() {
  var width = $('.top-nav')[0].getBoundingClientRect().width;
  $('.side-nav-button').sideNav({
    menuWidth: width,
    edge: 'right',
    closeOnClick: false,
    draggable: true,
    onOpen: null,
    onClose: null
  });
  $('.side-nav-button').on('click', function() {
    var width = $('.top-nav')[0].getBoundingClientRect().width;
    $('#slide-out').attr('style', `width: ${width}`);
    $('#sidenav-overlay').attr('style', 'background-color: rgba(0,0,0,0)');
  })
})